package com.puravida.groogle.test.sheet;

import com.google.api.services.drive.DriveScopes;
import com.google.api.services.sheets.v4.SheetsScopes;
import com.puravida.groogle.*;
import org.h2.jdbcx.JdbcDataSource;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;

public class SheetBuilderTest {

    Groogle groogle;
    SheetService sheetService;

    @Before
    public void setUp() {
        //tag::register[]
        groogle = GroogleBuilder.build(groogle -> {
            groogle
                    .withOAuthCredentials(credentials ->
                            credentials
                                    .storeCredentials(true)
                                    .applicationName("test-sheet")
                                    .withScopes(DriveScopes.DRIVE, SheetsScopes.SPREADSHEETS)
                                    .usingCredentials("src/test/resources/client_secret.json")
                    )
                    .service(DriveServiceBuilder.build(), DriveService.class)
                    .service(SheetServiceBuilder.build(), SheetService.class);
        });
        sheetService = groogle.service(SheetService.class);
        //end::register[]
        assert sheetService != null;
    }

    @Test
    public void build(){
        assert groogle.service(SheetService.class) != null;
    }


    @Test
    public void create(){
        //tag::create[]
        SheetService.WithSpreadSheet withSheet = sheetService.createSpreadSheet("test");
        //end::create[]
        assert withSheet != null;
        assert withSheet.getId() != "";

        DriveService drive = groogle.service(DriveService.class);
        drive.withFile(withSheet.getId(), withFile ->
            withFile.removeFromDrive()
        );
    }

    @Test
    public void createFolder(){
        //tag::createFolder[]
        final DriveService driveService = groogle.service(DriveService.class);
        final SheetService sheetService = groogle.service(SheetService.class);

        SheetService.WithSpreadSheet withSheet = sheetService.createSpreadSheet("test");

        final String newFolder = new SimpleDateFormat("yyyyMMddHHmm").format(new java.util.Date());
        final DriveService.WithFolder removeMe = driveService.createFolder(newFolder);

        driveService.withFile(withSheet.getId(), withFile ->
                withFile.moveToFolder(removeMe.getId())
        );

        removeMe.removeFromDrive();
        //end::createFolder[]
    }

    @Test
    public void withSpreadSheet(){
        final DriveService driveService = groogle.service(DriveService.class);
        final SheetService sheetService = groogle.service(SheetService.class);

        SheetService.WithSpreadSheet withSpreadSheet = sheetService.createSpreadSheet("test");

        withSpreadSheet.getSheets().forEach( (k, v)->
                System.out.println("sheet : " + k + " name : " + v));

        //tag::createSheet[]
        SheetService.WithSheet sheet = withSpreadSheet.createSheet("otra hoja");

        withSpreadSheet.getSheets().forEach( (k, v)->
                System.out.println("sheet : " + k + " name : " + v));

        int size1 = withSpreadSheet.getSheets().size();
        //end::createSheet[]

        //tag::removeSheet[]
        withSpreadSheet.removeSheet(sheet);
        //end::removeSheet[]

        int size2 = withSpreadSheet.getSheets().size();

        driveService.withFile(withSpreadSheet.getId(), withFile ->
                withFile.removeFromDrive()
        );

        assert size1 == 2;
        assert size2 == 1;

    }

    @Test
    public void withSpreadSheetConsumer(){
        final DriveService driveService = groogle.service(DriveService.class);
        final SheetService sheetService = groogle.service(SheetService.class);

        SheetService.WithSpreadSheet withSpreadSheet =
        //tag::createConsumer[]
                sheetService.createSpreadSheet("test", spread -> {
                    System.out.println( spread.getId() );
                    spread.createSheet("test", sheet ->{
                        sheet.duplicate("other sheet");
                    });
                });
        //end::createConsumer[]

        withSpreadSheet.getSheets().forEach( (k, v)->
                System.out.println("sheet : " + k + " name : " + v));

        int size = withSpreadSheet.getSheets().size();

        driveService.withFile(withSpreadSheet.getId(), withFile ->
                withFile.removeFromDrive()
        );

        assert size == 3;
    }



    @Test
    public void withSheet(){
        final DriveService driveService = groogle.service(DriveService.class);
        final SheetService sheetService = groogle.service(SheetService.class);

        SheetService.WithSpreadSheet withSpreadSheet =
        //tag::withSheet[]
        sheetService.createSpreadSheet("test", spreadSheet -> {
            spreadSheet.createSheet( "sheet", sheet->{
                sheet.getId();
            });
        });
        //end::withSheet[]

        driveService.withFile(withSpreadSheet.getId(), withFile ->
                withFile.removeFromDrive()
        );
    }


    @Test
    public void withCell(){
        final DriveService driveService = groogle.service(DriveService.class);
        final SheetService sheetService = groogle.service(SheetService.class);

        //tag::withCell[]
        SheetService.WithSpreadSheet withSpreadSheet =
                sheetService.createSpreadSheet("test", spreadSheet -> {
                    spreadSheet.createSheet( "sheet", sheet->{

                        sheet.cell("A1").set("Hola");

                        sheet.cell("B1", cell-> cell.set("=A1") );

                    });
                });

        assert withSpreadSheet.findSheet("sheet").cell("A1").get().equals("Hola");
        assert withSpreadSheet.findSheet("sheet").cell("B1").get().equals("Hola");
        //end::withCell[]

        driveService.withFile(withSpreadSheet.getId(), withFile ->
                withFile.removeFromDrive()
        );
    }

    @Test
    public void withRange(){
        final DriveService driveService = groogle.service(DriveService.class);
        final SheetService sheetService = groogle.service(SheetService.class);

        //tag::withRange[]
        SheetService.WithSpreadSheet withSpreadSheet =
                sheetService.createSpreadSheet("test", spreadSheet -> {
                    spreadSheet.createSheet( "sheet", sheet->{
                        sheet.writeRange("A1", "B2", range ->{

                            List row1 = Arrays.asList("Hola", "=A1");
                            List row2 = Arrays.asList(    22, "=A2");
                            List values = Arrays.asList(row1, row2);

                            range.set(values);
                        });

                    });
                });

        List<List<Object>> list = withSpreadSheet.findSheet("sheet").writeRange("A1", "B1").get();

        assert list.size() == 1;
        assert list.get(0).size() == 2;
        assert list.get(0).get(0).equals("Hola");
        assert list.get(0).get(1).equals("Hola");
        //end::withRange[]

        driveService.withFile(withSpreadSheet.getId(), withFile ->
                withFile.removeFromDrive()
        );
    }

    @Test
    public void appendDelimitedRange(){
        final DriveService driveService = groogle.service(DriveService.class);
        final SheetService sheetService = groogle.service(SheetService.class);

        //tag::appendDelimitedRange[]
        SheetService.WithSpreadSheet withSpreadSheet =
                sheetService.createSpreadSheet("test", spreadSheet -> {
                    spreadSheet.createSheet( "sheet", sheet->{

                        sheet.append("A", "Z", range ->{
                            for(int i=0; i<1001; i++) {
                                range.insert(Arrays.asList("Hola", "caracola", 22, "=A1"));
                            }
                        });

                    });
                });

        List<List<Object>> list = withSpreadSheet.findSheet("sheet").writeRange("A1", "D100").get();

        assert list.size() == 100;
        assert list.get(0).size() == 4;

        assert list.get(0).get(0).equals("Hola");

        assert list.get(99).get(1).equals("caracola");
        assert list.get(99).get(2).equals("22");
        assert list.get(99).get(3).equals("Hola");
        //end::appendDelimitedRange[]

        driveService.withFile(withSpreadSheet.getId(), withFile ->
                withFile.removeFromDrive()
        );
    }

    @Test
    public void appendRange(){
        final DriveService driveService = groogle.service(DriveService.class);
        final SheetService sheetService = groogle.service(SheetService.class);

        //tag::appendRange[]
        SheetService.WithSpreadSheet withSpreadSheet =
                sheetService.createSpreadSheet("test", spreadSheet -> {
                    spreadSheet.createSheet( "sheet", sheet->{

                        sheet.cell("B1").set("existing value");

                        sheet.append(range ->{
                            range.insert(Arrays.asList("Hola", "caracola", 22, "=A1"));
                            range.insert(Arrays.asList("Hola", "caracola", 22, "=A1"));
                        });

                    });
                });

        List<List<Object>> list = withSpreadSheet.findSheet("sheet").writeRange("A1", "D10").get();

        assert list.size() == 3;

        assert list.get(0).size() == 2;
        assert list.get(0).get(0).equals("");
        assert list.get(0).get(1).equals("existing value"); // B1

        assert list.get(2).get(0).equals("");
        assert list.get(2).get(1).equals("Hola");
        //end::appendRange[]

        driveService.withFile(withSpreadSheet.getId(), withFile ->
                withFile.removeFromDrive()
        );
    }

    @Test
    public void appendQuery()throws Exception{
        final DriveService driveService = groogle.service(DriveService.class);
        final SheetService sheetService = groogle.service(SheetService.class);

        //tag::appendSQL[]
        JdbcDataSource ds = new JdbcDataSource();
        ds.setURL("jdbc:h2:mem:testjava;DB_CLOSE_DELAY=-1");
        ds.setUser("");
        ds.setPassword("");
        Connection conn = ds.getConnection();
        Statement stmt = conn.createStatement();
        stmt.execute("create table user(id int primary key, name varchar(100), surname varchar(100) )");
        stmt.execute("insert into user values(1, 'hello', 'world')");
        stmt.execute("insert into user values(2, 'hola', 'mundo')");

        SheetService.WithSpreadSheet withSpreadSheet =
                sheetService.createSpreadSheet("test", spreadSheet -> {
                    spreadSheet.createSheet( "sheet", sheet->{

                        sheet.fromDataSource(consumer ->{
                            consumer
                                    .dataSource(ds)
                                    .query("select * from user")
                                    .range("B1");
                        });

                    });
                });

        //end::appendSQL[]
        driveService.withFile(withSpreadSheet.getId(), withFile ->
                withFile.removeFromDrive()
        );
    }

}
