= Groogle Sheeet
Jorge Aguilera <jorge.aguilera@puravida-software.com>
:description: Groogle Sheet DSL
:keyworks: groovy, google, groogle, dsl, sheet

Groogle Sheet es el DSL que nos permitirá interactuar con nuestras hojas de cálculo alojadas en Drive.

== Maven/Gradle

Para añadir las dependencias necesarias a tu proyecto usa las siguientes coordenadas:

[#maven,source,xml]
.pom.xml
----
<dependency>
  <groupId>com.puravida-software.groogle</groupId>
  <artifactId>groogle-sheet</artifactId>
  <version>2.0.0</version>
  <type>pom</type>
</dependency>
----

.build.gradle
[#gradle,source,groovy]
----
compile 'com.puravida-software.groogle:groogle-sheet:3.0.0'
----

== Registro

Para poder usar GroogleSheet en un entorno `autentificado` debemos registrar el servicio durante el
proceso de construcción de `groogle`:

[source,java]
.Register.java
----
include::{examplesdir}/com/puravida/groogle/test/sheet/SheetBuilderTest.java[tags=register]
----

[source,groovy]
.Register.groovy
----
include::{examplesdir}/com/puravida/groogle/test/sheet/GroovySheetBuilderSpec.groovy[tags=register]
----


WARNING: En el registro de Groogle sólo puede haber un servicio de cada tipo (clase)

== Uso

Una vez que el servicio(s) está registrado podemos acceder al mismo en cualquier parte de nuestro
programa a través del objeto `groogle` obtenido:

[#service,source,java]
----
sheet = groogle.service(SheetService.class);

----

Todas las solicitudes al servicio se realizarán usando la configuración definida durante el `build`

